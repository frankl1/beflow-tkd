import json
from flask import Flask, request, render_template

mode = 'bivariate' 
# mode = 'multivariate'

max_length = 32 # max length of time series
classes = ['ap_chagi', 'bandal_chagi', 'dwi_huryo_chagi']
classes2int = {c: i for i, c in enumerate(classes)}
int2classes = {i: c for i, c in enumerate(classes)}

if mode == 'bivariate':
    from tkd_bivariate import *
else:
    from tkd_multivariate import *

app = Flask(__name__)

@app.route('/')
def homepage():
	return render_template('index.html', pred=None)

@app.route('/predict/', methods=['POST'])
def predict():
    json_move = request.form['json_move']
    json_move = json_move.replace("'", '"')
    json_move = json.loads(json_move)
    formated_move = format_json_move(json_move, max_length)
    class_, confidence = predict_move(model, formated_move)
    return render_template('index.html', prediction=int2classes[class_], confidence=round(confidence, 2))

@app.route('/rest_api/predict/', methods=['POST'])
def rest_api_predict():
    try:
        json_move = request.json

        formated_move = format_json_move(json_move, max_length)
        class_, confidence = predict_move(model, formated_move)
        
        return {
            'prediction':int2classes[class_], 
            'confidence': f'{round(confidence, 2)}%'
        }
    except Exception as e:
        return {
            'prediction': 'A server error occurred',
            'confidence': '100%'
        }

if __name__ == '__main__':
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    # Flask's development server will automatically serve static files in
    # the "static" directory. See:
    # http://flask.pocoo.org/docs/1.0/quickstart/#static-files. Once deployed,
    # App Engine itself will serve those files as configured in app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)