#!/usr/bin/env python
# coding: utf-8

# In[1]:


import time
import numpy as np

from sklearn.linear_model import RidgeClassifierCV
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support

from joblib import load

from sast import *

key_points = ['leftAnkle', 'rightAnkle', 'leftEar', 'rightEar', 'leftElbow', 'rightElbow', 'leftHip', 'rightHip', 'leftKnee', 'rightKnee', 'leftShoulder', 'rightShoulder', 'leftWrist', 'rightWrist', 'nose']

def format_series(series, max_length):
    series_len = len(series)
    if series_len < max_length:
        series = np.concatenate([series, [np.mean(series)] * (max_length - series_len)])
    return np.array(series, dtype=np.float64)

def format_json_move(move, max_length):
    output_data = {}
    for kp in key_points:
        x_series = np.array(move[kp]['x'].split(','), dtype=np.float64)
        y_series = np.array(move[kp]['y'].split(','), dtype=np.float64)
        output_data[f'{kp}_x'] = format_series(x_series, max_length)
        output_data[f'{kp}_y'] = format_series(y_series, max_length)
    return output_data

def predict_move(model, move):
    predictions = []
    for key, m in model.items():
        predictions.append(m.predict_proba(np.expand_dims(move[key], axis=0)))
    predictions = np.mean(predictions, axis=0)
    return np.argmax(predictions), np.max(predictions) * 100


# # Load model

model = {}

start_time = time.time()

for key in key_points:
    model[f'{key}_x'] = load(f'models/multivariate/tkd_model_{key}_x_rocket.joblib')
    model[f'{key}_y'] = load(f'models/multivariate/tkd_model_{key}_y_rocket.joblib')
    
end_time = time.time()
print('Model loaded in', end_time - start_time, 'seconds')

