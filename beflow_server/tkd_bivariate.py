#!/usr/bin/env python
# coding: utf-8

# In[1]:


import time
import numpy as np
from sklearn.linear_model import RidgeClassifierCV
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support

from joblib import  load

from sast import *

key_points = ['leftAnkle', 'rightAnkle', 'leftEar', 'rightEar', 'leftElbow', 'rightElbow', 'leftHip', 'rightHip', 'leftKnee', 'rightKnee', 'leftShoulder', 'rightShoulder', 'leftWrist', 'rightWrist', 'nose']

def format_series(series, max_length):
    series_len = len(series)
    if series_len < max_length:
        queue = [np.mean(series)] * (max_length - series_len)
        return np.concatenate([series, queue])
    return series

def format_json_move(move, max_length):
    xaxis = []
    yaxis = []
    for kp in key_points:
        x_series = np.array(move[kp]['x'].split(','), dtype=np.float64)
        y_series = np.array(move[kp]['y'].split(','), dtype=np.float64)
        xaxis.append(format_series(x_series, max_length))
        yaxis.append(format_series(y_series, max_length))
    return {'x': np.concatenate(xaxis), 'y': np.concatenate(yaxis)}


def predict_move(model, move):
    pred_xaxis = model['model_xaxis'].predict_proba(np.expand_dims(move['x'], axis=0))
    pred_yaxis = model['model_yaxis'].predict_proba(np.expand_dims(move['y'], axis=0))
    pred = (pred_xaxis + pred_yaxis)/2
    return np.argmax(pred), np.max(pred)*100

# # Load the model

start_time = time.time()

model = {
    'model_xaxis' : load('models/tkd_model_xaxis_bivariate_rocket.joblib'),
    'model_yaxis' : load('models/tkd_model_yaxis_bivariate_rocket.joblib')
}

end_time = time.time()
print('Model loaded in', end_time - start_time, 'seconds')

# # Saving the models

