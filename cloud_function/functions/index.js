const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.kickCounter = functions.database.ref('moves/{kick_id}/{move_id}').onWrite((change, context) => {
	let kick_id = context.params.kick_id;

	let counterRef = admin.database()
					    .ref('counters')
					    .child(kick_id);

    return counterRef.transaction(current_value => {
	    // onCreate
	    if (!change.before.exists()){
	      return (current_value || 0) + 1
	    } else if (!change.after.exists()){
	      return (current_value || 0) - 1
	    }
	    
	});
});
