import 'package:beflowapp/model/game.dart';
import 'package:beflowapp/model/move.dart';
import 'package:beflowapp/widgets/beflowroom.dart';
import 'package:beflowapp/widgets/globals.dart';
import 'package:beflowapp/widgets/videoplayer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MoveOrder extends StatelessWidget {
  Set<Move> _listMoves;

  MoveOrder(Set<Move> moves){
    _listMoves = {};

    for (var i = 1; i <= moves.length; i++){
      Move m = moves.elementAt(i-1);
      m.label = i.toString();
      _listMoves.add(m);
    }
  }

  Widget buildRow(Move move) {
    return Card(
      child: Column(children: <Widget>[
        ListTile(
          title: Text(move.description),
          trailing: Text(move.label),
        ),
        Container(
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
          child: VideoPlayerScreen(
            videoPath: move.videoPath,
          ),
        )
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text('Move-Number association'),
        ),
        body: Column(children: <Widget>[
          Expanded(
              child: ListView.separated(
            itemCount: _listMoves.length,
            separatorBuilder: (context, i) => Divider(),
            itemBuilder: (context, i) {
              return buildRow(_listMoves.elementAt(i));
            },
          )),
          new RaisedButton(
            onPressed: () async {
              SharedPreferences.getInstance().then((prefs) {
                Game game = Game(
                    prefs.getInt(Globals.maxMovesPerSeries),
                    prefs.getInt(Globals.nbSecsBetweenSeries) * 1000,
                    _listMoves,
                    prefs.getString(Globals.username)
                );
                Navigator.push(context, MaterialPageRoute(
                    builder: (context) {
                      return BeFlowRoomWidget(game);
                    }
                ));
              });
            },
            textColor: Colors.white,
            color: Colors.lightBlueAccent,
            child: new Text("Start", style: Globals.validateButtonStyle),
          ),
        ]));
  }
}
