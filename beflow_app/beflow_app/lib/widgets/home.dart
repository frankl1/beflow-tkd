import 'package:beflowapp/widgets/posecam.dart';
import 'package:beflowapp/widgets/setup.dart';
import 'package:beflowapp/widgets/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Home extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    AudioManager.preload();
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              'Be Flow Tkd',
              style: Globals.titleStyle,
            ),
            IconButton(
                icon: Icon(
                  Icons.play_circle_outline,
                  color: Colors.blueAccent,
                ),
                iconSize: 72,
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => SetUp())
//                      MaterialPageRoute(builder: (context) => PoseCam())
                  );
                }
            )
          ],
        ),
      ),
    );
  }
}
