import 'package:beflowapp/model/move.dart';
import 'package:beflowapp/widgets/globals.dart';
import 'package:beflowapp/widgets/moveOrder.dart';
import 'package:beflowapp/widgets/videoplayer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SelectMoves extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text('Select your moves'),
        ),
        body: MoveListView());
  }
}

class MoveListView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MoveListViewState();
  }
}

class MoveListViewState extends State<MoveListView> {
  Set<Move> _listMoves = Move.movesList;
  Set<Move> _selectedMoves = <Move>{};

  Widget buildRow(Move move) {
    bool isSelected = _selectedMoves.contains(move);
    return Card(
      child: Column(children: <Widget>[
        ListTile(
          title: Text(move.description),
          trailing: isSelected
              ? Icon(Icons.favorite, color: Colors.red)
              : Icon(Icons.favorite_border),
          onTap: () {
            setState(() {
              if (isSelected) {
                _selectedMoves.remove(move);
              } else {
                _selectedMoves.add(move);
              }
            });
          },
        ),
        Container(
          padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
          child: VideoPlayerScreen(
            videoPath: move.videoPath,
          ),
        )
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    List<Widget> list = <Widget>[
      Expanded(
          child: ListView.separated(
        itemCount: _listMoves.length,
        separatorBuilder: (context, i) => Divider(),
        itemBuilder: (context, i) {
          return buildRow(_listMoves.elementAt(i));
        },
      ))
    ];
    if (_selectedMoves.length > 0) {
      list.add(RaisedButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => MoveOrder(_selectedMoves)));
          },
          textColor: Colors.white,
          color: Colors.lightBlueAccent,
          child: new Text("Continue", style: Globals.validateButtonStyle),
      ));
    }
    return Column(children: list);
  }
}
