import 'package:beflowapp/widgets/globals.dart';
import 'package:beflowapp/widgets/posecam.dart';
import 'package:beflowapp/widgets/selectMoves.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SetUp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
//    return SetUpForm();
    return Scaffold(
        appBar: AppBar(
          title: Text('Select move to detect'),
      ),
      body: Center(
        child: Column (
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: createListOfMoves(),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 30),
              child: RaisedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PoseCam(Globals.prediction_id, 'Prediction'))
                  );
                },
                textColor: Colors.white,
                color: Colors.lightBlueAccent,
                child: new Text("Predict moves", style: Globals.validateButtonStyle),
              ),
            )
          ],
        ),
      )
    );
  }
}

Widget createListOfMoves() {
  List<Map> moves = [
    {
      'name': 'Ap chagi',
      'counter': 0,
      'id': 'ap_chagi'
    },
    {
      'name': 'Bandal Chagi',
      'counter': 0,
      'id': 'bandal_chagi'
    },
    {
      'name': 'Naeryo Chagi',
      'counter': 0,
      'id': 'naeryo_chagi'
    },
    {
      'name': 'Kodeup Chagi',
      'counter': 0,
      'id': 'kodeup_chagi'
    },
    {
      'name': 'Dwi Huryo Chagi',
      'counter': 0,
      'id': 'dwi_huryo_chagi'
    }
  ];

  return ListView.separated(
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(moves[index]['name']),
              onTap: (){
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PoseCam(moves[index]['id'], moves[index]['name']))
                );
              },
            );
          },
          separatorBuilder: (context, index) => Divider(),
          itemCount: moves.length
  );
}

class SetUpForm extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SetUpFormState();
  }

}

class SetUpFormState extends State<SetUpForm>{
  final _formKey = GlobalKey<FormState>();

  TextEditingController _maxMovesPerSeriesCtlr = TextEditingController();
  TextEditingController _nbSecsBetweenSeriesCtlr = TextEditingController();
  TextEditingController _nameCtlr = TextEditingController();

  void saveSetup() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt(Globals.maxMovesPerSeries, int.parse(_maxMovesPerSeriesCtlr.text));
    prefs.setInt(Globals.nbSecsBetweenSeries, int.parse(_nbSecsBetweenSeriesCtlr.text));
    prefs.setString(Globals.username, _nameCtlr.text);
  }

  VoidCallback validate(BuildContext context){
    saveSetup();
    Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => SelectMoves())
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Set Up'),
      ),
      body: createForm(),
    );
  }

  Widget createForm(){
    return Form(
        key: _formKey,
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          children: <Widget>[
            TextFormField(
              decoration: const InputDecoration(
                  icon: Icon(Icons.person),
                  labelText: 'Name',
                  labelStyle: Globals.labelStyle
              ),
              controller: _nameCtlr,
              validator: (value) {
                if (value.isEmpty){
                  return 'Please enter some text';
                }
                return null;
              },
            ),
            TextFormField(
              decoration: const InputDecoration(
                  icon: const Icon(Icons.edit),
                  labelText: 'Max moves per series',
                  labelStyle: Globals.labelStyle
              ),
              controller: _maxMovesPerSeriesCtlr,
              keyboardType: TextInputType.number,
              inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
              validator: (value) {
                if (value.isEmpty) {
                  return 'Enter the max number of moves per series';
                }
                return null;
              },
            ),
            TextFormField(
              decoration: const InputDecoration(
                  icon: Icon(Icons.timer),
                  labelText: 'Nb secs between series',
                  labelStyle: Globals.labelStyle
              ),
              controller: _nbSecsBetweenSeriesCtlr,
              keyboardType: TextInputType.number,
              inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
              validator: (value) {
                if (value.isEmpty) {
                  return 'Enter the number of seconds between two series';
                }
                int i = int.parse(value);
                if (i <=0 || i > 4 ){
                  return 'Enter an integer between 0 and 4';
                }
                return null;
              },
            ),
            Padding(
              padding: EdgeInsets.only(top: 20, left: 36),
              child: new RaisedButton(
                onPressed: () {
                  if(_formKey.currentState.validate()){
                    validate(context);
                  }
                },
                textColor: Colors.white,
                color: Colors.lightBlueAccent,
                child: new Text(
                    "Continue",
                    style: Globals.validateButtonStyle
                ),
              ),
            )
          ],
        )
    );
  }

}