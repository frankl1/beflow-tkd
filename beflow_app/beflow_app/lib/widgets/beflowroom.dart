import 'dart:async';

import 'package:beflowapp/model/game.dart';
import 'package:beflowapp/model/move.dart';
import 'package:beflowapp/model/series.dart';
import 'package:beflowapp/widgets/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BeFlowRoomWidget extends StatelessWidget {
  Game _game;

  BeFlowRoomWidget(this._game);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Now be flow'),
      ),
      body: BeFlowRoom(_game),
    );
  }
}

class BeFlowRoom extends StatefulWidget {
  Game _game;

  BeFlowRoom(this._game);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return BeFlowRoomState(_game);
  }
}

class BeFlowRoomState extends State<BeFlowRoom> {
  var _seriesNumber;
  bool _pause;
  Series _series;
  Game _game;
  Timer _timer;
  Duration _duration;
  Duration _lastSeriesTime;

  BeFlowRoomState(this._game);

  @override
  void initState() {
    _seriesNumber = 0;
    _duration = Duration.zero;
    _lastSeriesTime = Duration.zero;
    _pause = true;
    _game.playMotivation();
    play();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  format(Duration d) => d.toString().split('.').first;

  void pause() {
    _timer.cancel();
    setState(() {
      _pause = true;
    });
  }

  void restart() {
    _timer.cancel();
    initState();
  }

  void play() {
    Duration oneSec = Duration(milliseconds: 60);
    _timer = Timer.periodic(oneSec, (t) {
      _pause = false;
      _duration += oneSec;
      if (_duration - _lastSeriesTime >= _game.durationBetweenSeries) {
        if (_game != null) {
          if(_series != null) _series.dispose();
          setState(() {
            _series = _game.generateNextSeries();
            print("Series: $_series");
            _seriesNumber++;
            _series.play();
            _lastSeriesTime = Duration(milliseconds: _duration.inMilliseconds);
          });
        } else {
          setState(() { });
        }
      } else {
        setState(() { });
      }
//      _series?.play();
    });
  }

  Widget setUpWidget() {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              '${_game?.playerName}',
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text('Max length: ${_game?.maxMovePerSeries}'),
            Text('Step length: ${_game?.durationBetweenSeries?.inSeconds} sec')
          ],
        ),
      ),
    );
  }

  Widget seriesNumberWidget() {
    return Card(
        child: Padding(
      padding: EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          Text('Series count'),
          Text(
            _seriesNumber.toString(),
            style: Globals.timerStyle,
          )
        ],
      ),
    ));
  }

  Widget stopWatch() {
    return Card(
      child: Center(
        child: Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                Text('Elapsed time'),
                Text(
                  format(_duration),
                  style: Globals.timerStyle,
                ),
              ],
            )),
      ),
    );
  }

  Widget seriesWidget() {
    var seriesText = '';
    _series?.moves?.forEach((m) {
      seriesText += (seriesText.length == 0) ? '${m.label}' : '-${m.label}';
    });
    return Card(
      child: Center(
        child: Padding(
            padding: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                Text('Series'),
                Text(seriesText, style: Globals.seriesStyle),
              ],
            )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            stopWatch(),
            Expanded(
              child: seriesNumberWidget(),
            ),
            setUpWidget(),
          ],
        ),
        seriesWidget(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            IconButton(
              icon: Icon(
                _pause ? Icons.play_arrow : Icons.pause,
                color: Colors.blue,
                size: 60,
              ),
              onPressed: () {
                _pause ? play() : pause();
              },
            ),
            IconButton(
              icon: Icon(
                Icons.refresh,
                color: Colors.grey,
                size: 60,
              ),
              onPressed: () {
                pause();
                restart();
              },
            )
          ],
        )
      ],
    );
  }
}
