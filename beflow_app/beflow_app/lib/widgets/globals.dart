import 'dart:core';
import 'dart:core';
import 'dart:ui';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Globals {
  static List<CameraDescription> cameras;
  // Styles
  static const TextStyle validateButtonStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.w800,
  fontFamily: 'Roboto',
  letterSpacing: 0.5,
  fontSize: 16,
  height: 1,
  );

//  static const String server_address = 'http://104.154.133.208/rest_api/predict/';
  static const String server_address = 'http://192.168.43.116:5000/rest_api/predict/';

  static const String prediction_id = 'predict';

  static const TextStyle labelStyle = TextStyle(
  fontSize: 12,
  );

  static const TextStyle timerStyle = TextStyle(
    fontSize: 30,

  );

  static const TextStyle titleStyle = TextStyle(
    color: Colors.blue,
    fontWeight: FontWeight.w800,
    fontFamily: 'Courier',
    letterSpacing: 0.5,
    fontSize: 30,
    height: 2,
  );

  static const TextStyle seriesStyle = TextStyle(
    fontSize: 30,
  );

  // Set up
  static const username = 'username';
  static const maxMovesPerSeries = 'max moves per series';
  static const nbSecsBetweenSeries = 'sec between series';

  static const String posenet_mv1_kpt = 'assets/models/posenet_mv1_075_float_from_checkpoints.tflite';
  static const String posenet_mv1_multi_kpt = 'assets/models/posenet_mobilenet_v1_100_257x257_multi_kpt_stripped.tflite';
}

class AudioManager {
  static const numbers = {
    '1': 'One.m4a',
    '2': 'Two.m4a',
    '3': 'Three.m4a',
    '4': 'Four.m4a'
  };

  static const motivations = {
    '0': 'Gogogo.m4a',
    '1': 'Keep-moving.m4a'
  };

  static final AudioCache audioPlayer = AudioCache(prefix: 'audios/', fixedPlayer: AudioPlayer(mode: PlayerMode.LOW_LATENCY));

  static void preload(){
    List<String> files = [];
    motivations.forEach((k, v) {
      files.add(v);
    });
    numbers.forEach((k, v) {
      files.add(v);
    });
    audioPlayer.clearCache();
    audioPlayer.loadAll(files);
  }

  static Future<AudioPlayer> play(filename) {
    return audioPlayer.play(filename);
  }
}