import 'dart:async';
import 'dart:math' as math;

import 'package:beflowapp/model/dbmove.dart';
import 'package:beflowapp/widgets/bndbox.dart';
import 'package:beflowapp/widgets/globals.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:hardware_buttons/hardware_buttons.dart';
//import 'package:speech_to_text/speech_to_text.dart';
import 'package:tflite/tflite.dart';

typedef void Callback(List<dynamic> list, int h, int w);

class PoseCam extends StatefulWidget {
  String moveId;
  String moveName;

  PoseCam(this.moveId, this.moveName);

  @override
  PoseCamState createState() => new PoseCamState();
}

class PoseCamState extends State<PoseCam> {
  CameraController controller;
  bool isDetecting = false;
  bool isExecuting = false;
  bool isSavingOrPredicting = false;
  List<dynamic> _recognitions;
  int _imageHeight = 0;
  int _imageWidth = 0;
  int camNumber = 0;
  DbMove dbMove;
  StreamSubscription volumeButtonSubscription;
//  SpeechToText _speech;

  @override
  Widget build(BuildContext context) {
    if (controller == null || !controller.value.isInitialized) {
      return Container();
    }

    var screen = MediaQuery.of(context).size;
    var screenH = math.max(screen.height, screen.width);
    var screenW = math.min(screen.height, screen.width);
    var tmp = controller.value.previewSize;
    var previewH = math.max(tmp.height, tmp.width);
    var previewW = math.min(tmp.height, tmp.width);
    var screenRatio = screenH / screenW;
    var previewRatio = previewH / previewW;

    return Scaffold(
        body: Builder(
      builder: (context) => Stack(
        children: <Widget>[
          OverflowBox(
            maxHeight: screenRatio > previewRatio
                ? screenH
                : screenW / previewW * previewH,
            maxWidth: screenRatio > previewRatio
                ? screenH / previewH * previewW
                : screenW,
            child: CameraPreview(controller),
          ),
          BndBox(
              _recognitions == null ? [] : _recognitions,
              math.max(_imageHeight, _imageWidth),
              math.min(_imageHeight, _imageWidth),
              screen.height,
              screen.width),
          Center(child: actionButton(context)),
          Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: EdgeInsets.only(top: 40),
                child: Text(
                  widget.moveId != Globals.prediction_id ?  'Reading ${widget.moveName}' : 'Making prediction',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
              )),
        ],
      ),
    ));
  }
  
  num round(num x){
    return num.parse(x.toStringAsFixed(3));
  }

  Future<void> _showMyDialog(prediction, confidence) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Prediction result'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Prediction: $prediction'),
                Text('Confidence: $confidence'),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton.icon(
              label: Text('Incorrect'),
              icon: Icon(Icons.close),
              color: Colors.red,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            RaisedButton.icon(
              label: Text('Correct'),
              icon: Icon(Icons.check),
              color: Colors.green,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  saveOrPredict(context) {
    if (widget.moveId == Globals.prediction_id){
      dbMove.predictMove((resp, hasFailed) {
        if (!hasFailed) {
          _showMyDialog(resp['prediction'], resp['confidence']);
        } else {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text("$resp"),
            backgroundColor: Colors.red,
            duration: Duration(seconds: 5),
          ));
        }
        setState(() {
          isSavingOrPredicting = false;
          dbMove = null;
        });
      });
    } else {
      dbMove.writeInDb(() {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Successfully written in DB'),
          backgroundColor: Colors.green,
          duration: Duration(seconds: 3),
        ));
        setState(() {
          isSavingOrPredicting = false;
          dbMove = null;
        });
      }, (error) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Failed  written in DB'),
          backgroundColor: Colors.red,
          duration: Duration(seconds: 5),
        ));
        setState(() {
          isSavingOrPredicting = false;
          dbMove = null;
        });
      });
    }
  }

  updateActionButtons() {
    if (isExecuting){
      return Icon(
        Icons.pause,
        size: 60,
        color: Colors.white,
      );
    } else {
      return Icon(
        Icons.play_arrow,
        size: 60,
        color: Colors.white,
      );
    }
  }

  void handleAction(context) {
    setState(() {
      if (!isExecuting) {
        runPoseNet();
      } else {
        if (controller.value.isStreamingImages) {
          controller.stopImageStream();
        }
        if (dbMove != null) {
          if (dbMove.confidences.isEmpty){
            Scaffold.of(context).showSnackBar(SnackBar(
              content: Text("No confident move detected"),
              backgroundColor: Colors.red,
              duration: Duration(seconds: 5),
            ));
            dbMove = null;
          } else {
            isSavingOrPredicting = true;
            saveOrPredict(context);
          }
        }
      }
      isExecuting = !isExecuting;
    });
  }

  void switchCamera() {
    setState(() {
      camNumber = (camNumber + 1) % Globals.cameras.length;
      initCamera();
    });
  }

  void handleSpeech(context, speechResult) {
    print('Speech COmmand $speechResult');
    if(speechResult == 'start' || speechResult == 'stop'){
      handleAction(context);
    }
    if (speechResult == 'switch'){
      switchCamera();
    }
  }

  Widget actionButton(context) {
    volumeButtonSubscription?.cancel();
    volumeButtonSubscription = volumeButtonEvents.listen((event) {
      if (event == VolumeButtonEvent.VOLUME_UP) {
        handleAction(context);
      } else {
        switchCamera();
      }
    });

//    if (!_speech.isNotListening) {
//      print('SPEECH state ${_speech.isListening}');
//      _speech.listen(
//          onResult: (result) => handleSpeech(context, result.recognizedWords));
//      print('+SPEECH state ${_speech.isListening}');
//    }

    if (isSavingOrPredicting){
      return ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Container(
          width: 200,
          height: 10,
          child: LinearProgressIndicator(),
        ),
      );
    }
    return RaisedButton(
      color: Colors.transparent,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
          side: BorderSide(color: Colors.white)),
      onPressed: () => handleAction(context),
      child: updateActionButtons(),
    );
  }

  void initCamera() {
    if (Globals.cameras == null || Globals.cameras.length < 1) {
      print('No camera is found');
    } else {
      print("Cameras: ${Globals.cameras}");
      controller = new CameraController(
        Globals.cameras[camNumber],
        ResolutionPreset.low,
      );
      controller.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {});
      });
    }
  }

//  void initSpeechToText() async {
//    _speech = SpeechToText();
//    bool available = await _speech.initialize(
//      finalTimeout: Duration(seconds: 5),
//      onStatus: (state) {
//        print('SPEECH state: $state');
//      },
//      onError: (error) {
//        print('SPEECH error: $error');
//      }
//    );
//    if (available) {
//      print('SPEECH available is available');
//    } else {
//      print('SPEECH not available');
//    }
//  }

  @override
  void initState() {
    super.initState();
    
    loadModel();
    initCamera();
//    initSpeechToText();
  }

  @override
  void dispose() {
    controller?.dispose();
    volumeButtonSubscription?.cancel();
//    _speech?.cancel();
    Tflite.close();
    super.dispose();
  }

  void loadModel() async {
    String res = await Tflite.loadModel(
        model: Globals.posenet_mv1_kpt,
        numThreads: 4
    );
    print("Loaded model: $res");
  }

  void runPoseNet() {
    dbMove = DbMove(widget.moveId);
    controller.startImageStream((CameraImage img) {
      if (!isDetecting) {
        isDetecting = true;

        int startTime = new DateTime.now().millisecondsSinceEpoch;

        Tflite.runPoseNetOnFrame(
          bytesList: img.planes.map((plane) {
            return plane.bytes;
          }).toList(),
          imageHeight: img.height,
          imageWidth: img.width,
          threshold: 0.7,
          numResults: 1,
          nmsRadius: 10
        ).then((recognitions) {
          int endTime = new DateTime.now().millisecondsSinceEpoch;
          print("Detection took ${endTime - startTime}");
          print(recognitions);

          setRecognitions(recognitions, img.height, img.width);
          updateDbMove(recognitions);

          isDetecting = false;
        });
      }
    });
  }

  setRecognitions(recognitions, imageHeight, imageWidth) {
    setState(() {
      _recognitions = recognitions;
      _imageHeight = imageHeight;
      _imageWidth = imageWidth;
    });
  }

  updateDbMove(recognitions) {
    recognitions.forEach((reg) {
      dbMove.confidences.add(round(reg['score']));
      reg['keypoints'].values.forEach((kp) {
        var kpName = kp['part'];
        var x = round(kp['x']);
        var y = round(kp['y']);
        var score = round(kp['score']);
        if (!dbMove.kpSeries.containsKey(kpName)) {
          dbMove.kpSeries[kpName] = KeyPointSeries();
        }
        dbMove.kpSeries[kpName].addKeyPoint(x, y, score);
      });
    });
  }
}
