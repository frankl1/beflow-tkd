import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:beflowapp/widgets/globals.dart';
import 'package:firebase_database/firebase_database.dart';
import 'dart:math';
import 'package:http/http.dart' as http;

class KeyPointSeries {
  List<double> _x;
  List<double> _y;
  List<double> _confidence;

  KeyPointSeries() {
    _x = [];
    _y = [];
    _confidence = [];
  }

  void addKeyPoint(x, y, score) {
    _x.add(x);
    _y.add(y);
    _confidence.add(score);
  }

  Map toMap() {
    return {
      'x': _x.join(','),
      'y': _y.join((',')),
      'confidence': _confidence.join(',')
    };
  }
}

class DbMove {
  List<double> confidences;
  Map<String, KeyPointSeries> kpSeries;
  String name;
  static final rootRef = FirebaseDatabase.instance.reference();

  DbMove(name) {
    confidences = [];
    kpSeries = {};
    this.name = name;
  }

  formatData() {
    Map data = {
      'confidence': confidences.join(",")
    };
    kpSeries.forEach((kp, series) {
      data[kp] = series.toMap();
    });
    return data;
  }

  writeInDb(successCallback, errCallback) {
    Map data = formatData();
    print('Wrinting $data in database');
    rootRef.child("moves").child(name).push().set(data).then((v) {
      successCallback();
    }).catchError((err) {
      errCallback(err);
      print(err);
    });
  }

  predictMove(respCallback) async{
    Map data = formatData();
    print('Predicting $data');

    try {
      final http.Response response = await http.post(
          Globals.server_address,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(data)
      ).timeout(const Duration(seconds: 60));

      if (response.statusCode == 200) {
        respCallback(json.decode(response.body), false);
      } else {
        respCallback(json.decode(response.body), true);
      }
    } on TimeoutException catch(e) {
      respCallback("Server timeout", true);
    } on Exception catch (e) {
      respCallback(e.toString(), true);
    }
  }
}