class BodyPart {
  String name;
  int id;
  double x;
  double y;
  double score;

  BodyPart(this.id, this.name, this.x, this.y, this.score);
}