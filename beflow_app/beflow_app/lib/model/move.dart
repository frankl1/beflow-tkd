class Move {
  var _description;
  var _videoPath;
  var _label;

  Move(this._description, this._videoPath, this._label);

  static Set<Move> movesList = <Move>{
    Move('Move1', 'assets/videos/move1.mp4', 1),
    Move('Move2', 'assets/videos/move4.mp4', 2),
    Move('Move3', 'assets/videos/move2.mp4', 3),
    Move('Move4', 'assets/videos/move3.mp4', 4),
  };


  get description => _description;

  void play(){

  }

  get videoPath => _videoPath;
  get label => _label;

  set label(value) {
    _label = value;
  }

  @override
  String toString() {
    return _label;
  }


}