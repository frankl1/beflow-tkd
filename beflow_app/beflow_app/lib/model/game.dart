import 'dart:io';
import 'dart:math';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:beflowapp/model/move.dart';
import 'package:beflowapp/model/series.dart';
import 'package:beflowapp/widgets/globals.dart';

enum GameState {PLAYING, PAUSE, NOT_STARTED}

class Game {
  int _maxMovePerSeries;
  Duration _durationBetweenSeries;
  Set<Move> _possibleMoves;
  Random _randomNumberGen;
  GameState _state;
  String _playerName;

  Game(int maxMovePerSeries, int nbMilliSecBetweenSeries, Set<Move> possibleMoves, String playerName){
    _maxMovePerSeries = maxMovePerSeries;
    _durationBetweenSeries = Duration(milliseconds: nbMilliSecBetweenSeries);
    _possibleMoves = possibleMoves;
    _randomNumberGen = Random();
    _state = GameState.NOT_STARTED;
    _playerName = playerName;
  }

  Series generateNextSeries(){
    var limit = _randomNumberGen.nextInt(_maxMovePerSeries)+1;
    Series s = Series();
    s.moves.addAll(List<Move>.generate(limit, (i) => _possibleMoves.elementAt(_randomNumberGen.nextInt(_possibleMoves.length))));
    return s;
  }

  Duration get durationBetweenSeries => _durationBetweenSeries;

  String get playerName => _playerName;

  int get maxMovePerSeries => _maxMovePerSeries;

  void playMotivation() {
    int motivation = _randomNumberGen.nextInt(AudioManager.motivations.length);
    AudioManager.play(AudioManager.motivations[motivation.toString()]);
  }

}