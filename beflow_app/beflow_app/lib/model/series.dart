import 'dart:async';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:beflowapp/model/move.dart';
import 'package:beflowapp/widgets/globals.dart';

class Series{
  List<Move> _moves;
  Timer _timer;
  Stopwatch _stopwatch;
  static Duration readInterval = Duration(milliseconds: 10);
  var _next2play;

  Series(){
    this._moves = <Move>[];
    _next2play = 0;
  }

  void addMove(Move move) {
    this._moves.add(move);
  }

  void deleteMove(Move move){
    this._moves.remove(move);
  }

  List<Move> get moves => _moves;

  void play () {
    _timer = Timer.periodic(readInterval, (t){
      if (_next2play < _moves.length && this != null){
        AudioManager.play(AudioManager.numbers[_moves.elementAt(_next2play).label.toString()]).then((v) {
          _next2play++;
        });
      } else {
        _timer.cancel();
      }
    });
  }

  void dispose(){
    _timer?.cancel();
  }

  @override
  String toString() {
    return _moves.map((m) => '${m.toString()}-').toString();
  }


}