import Webcam from 'react-webcam';
import React, {useState, useCallback, useEffect, useRef} from 'react';
import { Button } from '@material-ui/core';
import RadioButtonCheckedIcon from '@material-ui/icons/RadioButtonChecked';

import '@tensorflow/tfjs-backend-cpu';
import * as posenet from '@tensorflow-models/posenet';

function WebcamRecorder() {
    const [isRecording, setIsRecording] = useState(false);
    const [btnColor, setBtnColor] = useState('disabled');
    const [btnText, setBtnText] = useState('Start recording');
    const [poses, setPoses] = useState([]);
    const webCamRef = useRef(null);
    const canvasRef = useRef(null);
    const intervalRef = useRef(null);
    const timestep = 1000 // number of milliseconds between poses
    const minPoseConfidence = 0.1;
    const videoHeight = 400;
    const videoWidth = 400;
    const kpRadius = 2 // point radius

    const buttonStyle = {
        top: videoWidth,
    }

    const videoConstraints = {
        facingMode: 'user',
      }
      
    async function estimagePoseOnVideo(video) {
        return posenet.load({
            architecture: 'MobileNetV1',
            outputStride: 16,
            inputResolution: {width: videoWidth, height: videoHeight},
            multiplier: 0.5
        }).then((net) => {
            const pose = net.estimateSinglePose(video, {
                flipHorizontal: true
            });
                
            return pose;
        })
    }

    async function drawPose(pose) {
        if (pose.score < minPoseConfidence)
            return;

        const ctx = canvasRef.current.getContext('2d');

        ctx.clearRect(0, 0, canvasRef.current.width, canvasRef.current.height);
        
        const parts = {};
        pose.keypoints.forEach((kp) => {
            parts[kp.part] = {x: kp.position.x, y:kp.position.y}
            ctx.beginPath();
            ctx.arc(kp.position.x, kp.position.y, kpRadius, 0, 2*Math.PI);
            ctx.fillStyle = 'blue';
            ctx.fillText(kp.part, kp.position.x, kp.position.y);
            ctx.fill();
        });
    }

    useEffect(() => {
        setBtnColor(isRecording? 'secondary' : 'disabled');
        setBtnText(isRecording? 'Stop recording' : 'Start recording');

        if (isRecording) {
            console.log('About to start recording')

            intervalRef.current = setInterval(() => {
                estimagePoseOnVideo(webCamRef.current.video).then((pose) => {
                    setPoses(prev => prev.concat(pose));
                    drawPose(pose);
                    console.log('new pose', poses.length, ':', pose);
                })
            }, timestep);

        } else {
            if (intervalRef.current){
                clearInterval(intervalRef.current);
            }
            console.log('recording end')
        }

        return () => {
            if (intervalRef.current){
                clearInterval(intervalRef.current);
            }
        }
    }, [isRecording])

    return (
        <div className='webcam-div'>
            <Webcam 
                ref={webCamRef} 
                audio={false}
                videoConstraints={videoConstraints} 
                height={videoHeight} 
                width={videoWidth}
                className="webcam"
            />
            <canvas ref={canvasRef} className='canvas' width={videoWidth} height={videoHeight}></canvas>
            <div>
                <Button 
                    variant="contained" 
                    color="primary" 
                    endIcon={<RadioButtonCheckedIcon color={btnColor} />}
                    onClick={() => setIsRecording(!isRecording)}
                    style={buttonStyle}
                > {btnText} </Button>
            </div>
        </div>
    )
}

export default WebcamRecorder;