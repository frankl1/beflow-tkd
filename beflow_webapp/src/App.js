import './App.css';
import React from 'react';
import { AppBar, Grid, Toolbar, Typography } from '@material-ui/core';
import WebcamRecorder from './components/webcam';

const titleStyle = {
  flexGrow: 1,
}

function App() {

  return (
    <div>
      <AppBar position='static'>
        <Toolbar>
          <Typography variant="h6" style={titleStyle}>
            Tkd Moves Prediction
          </Typography>
        </Toolbar>
      </AppBar>

      <Grid container>
        <Grid item sm={8} >
          <WebcamRecorder />
        </Grid>
        
        <Grid item sm={4}>
          Config
        </Grid> 
      </Grid>
    </div>
  );
}

export default App;