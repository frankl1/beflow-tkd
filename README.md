# Tkd moves classification

The goal of this work is to design and build a framework in order to classify Taekwondo moves. To achieve this goal we have

1. Build the Tkd moves dataset
2. Design and build a classification model
   1. Dataset understanding
   2. Data preparation and feature selection
   3. Modeling
   4. Deployment

### Building the dataset

The dataset is built using the pose detection model Posenet. With this model, a taekwondo move can be represented as a multidimensional time series. Each dimension represents the time series of values taken by a **key point** (as view by Posenet)  during the execution of the move. There are **$17$ key points**:

| Id   | Part          |
| :--- | :------------ |
| 0    | nose          |
| 1    | leftEye       |
| 2    | rightEye      |
| 3    | leftEar       |
| 4    | rightEar      |
| 5    | leftShoulder  |
| 6    | rightShoulder |
| 7    | leftElbow     |
| 8    | rightElbow    |
| 9    | leftWrist     |
| 10   | rightWrist    |
| 11   | leftHip       |
| 12   | rightHip      |
| 13   | leftKnee      |
| 14   | rightKnee     |
| 15   | leftAnkle     |
| 16   | rightAnkle    |

Each key point is represented by an x and an y, hence there are 34 dimensions in total. 

#### Dataset building process

![Data building process](./images/Dataset building.jpg)

##### Setup

- $numThreads=4$
- $confidenceThreshold = 0.7$
- $ResolutionPreset = low$
- Phone: Xiaomi Redmi Note 9s



#### Database design

<img src="./images/db-structure.png" alt="db-structure" style="zoom:50%;" />

### Design and build the model

#### Data understanding

- A move is a time series of poses (an hierarchical multivariate) identified by key points or joint points
  
  - Each key points of a pose is represented by its $x$ coordinate and its $y$ coordinate
  - Each key point of a move is represented by the time series of the $x$ coordinate and the time series of the $y$ coordinate
  
  ![](./images/sample_ap_chagi.jpg)
  
- Each time series is of length at most $25$

- Two kicks: Ap chagi, Bandal chagi

- Number instance per kick: $20$ with using right foot and $20$ using the left foot

![](./images/sample_viz.jpg)



#### Data preparation and feature selection

- Ignored key points: right and left eyes
- Data transformation
  - input missing values with the mean of known values
  - Multivariate time series classification
  - Bivariate time series classification: convert each move to a bivariate time series of dimensions $x$ and $y$ where the $d$ dimension is the concatenation of the $d$ dimension of each key points of the move
  - Univariate time series classification: Concatenate the $x$ and $y$ dimensions of the previous bivariate representation.

#### Modeling

- Train a classifier on each dimension and average their prediction to get the final prediction

- Classifier candidates:
  - SAST
    - max_shp_length=max_move_length
  - Rocket

#### Deployment

![](./images/prediction-process.jpg)

The webserver runs a Flask webapp (contained in the folder beflow_server) which listens to moves and calls the trained model to make prediction.
